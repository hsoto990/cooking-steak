package minded;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JToggleButton;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class steak extends JApplet {

	String Modema = "Grill";

	public void paint(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, 200, 200);
		if (cookingHeated < 45)// Cruda
			g.setColor(new Color(184, 56, 53));
		else if (cookingHeated < 53)// Medio Cruda
			g.setColor(new Color(166, 65, 53));
		else if (cookingHeated < 60)// Termino medio
			g.setColor(new Color(177, 104, 87));
		else if (cookingHeated < 70)// Tres cuartos
			g.setColor(new Color(183, 122, 101));
		else if (cookingHeated < 75)// Bien hecha
			g.setColor(new Color(194, 147, 121));
		else if (cookingHeated < 130)// Sobrecocida
			g.setColor(new Color(175, 128, 102));
		else if (cookingHeated < 150)// SobreSobrecocida
			g.setColor(new Color(147, 98, 58));
		else if (cookingHeated < 180)// Searing
			g.setColor(new Color(133, 65, 26));
		else // Quemada
		{
			if (hotness < 3500)
				g.setColor(new Color(52, 27, 22));
			else if (hotness < 4500)
				g.setColor(new Color(200, 255, 150));
			else
				g.setColor(new Color(0, 0, 0, 10));

		}
		g.fillRect(50, 50, 100, 30);

		g.setColor(new Color(184, 56, 53));
		g.drawString(((int) (hotness * 100)) / 100.0 + "°C", 10, 10);
		g.setColor(Color.BLUE);
		g.drawString(((int) (wetness * 100)) / 100.0 + "% Wet", 10, 25);

	}

	/**
	 * Create the applet.
	 */
	double cookingHeated = 0;
	double hotness = 25;
	double wetness = 25;
	double HeatConductivity = 1;

	double HeatingSource = 100;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	public steak() {
		getContentPane().setLayout(null);

		JButton btnSalt = new JButton("Salt");
		btnSalt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				wetness/=2;
			}
		});
		btnSalt.setBounds(278, 124, 100, 25);
		getContentPane().add(btnSalt);

		JButton btnWash = new JButton("Wash");
		btnWash.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				wetness+=20;
			}
		});
		btnWash.setBounds(278, 161, 100, 25);
		getContentPane().add(btnWash);

		JToggleButton tglbtnOven = new JToggleButton("Grill");
		buttonGroup.add(tglbtnOven);
		tglbtnOven.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (tglbtnOven.isSelected()) {
					Modema = "Grill";
					HeatingSource = 200;
					HeatConductivity = 1;
				}
			}
		});
		tglbtnOven.setBounds(278, 12, 100, 25);
		getContentPane().add(tglbtnOven);

		JToggleButton tglbtnBoil = new JToggleButton("Boil");
		buttonGroup.add(tglbtnBoil);
		tglbtnBoil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (tglbtnBoil.isSelected()) {
					Modema = "Boil";
					HeatingSource = 100;
					HeatConductivity = 0.4;
				}
			}
		});
		tglbtnBoil.setBounds(278, 49, 100, 25);
		getContentPane().add(tglbtnBoil);

		JToggleButton tglbtnTable = new JToggleButton("Table");
		buttonGroup.add(tglbtnTable);
		tglbtnTable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tglbtnTable.isSelected()) {
					Modema = "Table";
					HeatingSource = 20;
					HeatConductivity = 0.1;
				}
			}
		});
		tglbtnTable.setBounds(278, 87, 100, 25);
		getContentPane().add(tglbtnTable);
		
		JToggleButton toggleButton = new JToggleButton("Ice Box");
		toggleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (toggleButton.isSelected()) {
					Modema = "Ice Box";
					HeatingSource = -20;
					HeatConductivity = 0.4;
				}
			}
		});
		buttonGroup.add(toggleButton);
		toggleButton.setBounds(278, 198, 100, 25);
		getContentPane().add(toggleButton);
		new Thread() {
			public void run() {
				while (true) {
					
					//Hydrating when boiling
					if (Modema.equals("Boil")) {
						if (hotness > 0 & hotness < 100)
							wetness += 1;
					}
					
					hotness+=HeatConductivity*((HeatingSource-hotness))/100;
					

					//Vaporization
					if (hotness > 0)
						wetness -= hotness / 1000;
					if (wetness < 0)
						wetness = 0;
					
					if (wetness > 100)
						wetness = 100;
					
					//Chemical Process
					if (hotness > cookingHeated)
						cookingHeated = hotness;
					repaint();
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
					}
				}
			}
		}.start();

	}
}
